<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'thrive' );

/** MySQL database username */
define( 'DB_USER', 'thrive' );

/** MySQL database password */
define( 'DB_PASSWORD', '8ca0Wlm4GQxE1SsO' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'SCJZZr_EiD[s:)P0~,J/CHUgbUF^D]X!PifU=3p[~M_ZD1~vfl|Kt58gm:%++a:l' );
define( 'SECURE_AUTH_KEY',  '/@q{T/Ms*{}nO! /{ur/xy]*MBp&}Ri|>&u*o2fGuTIvW]As8!=$;@}vs6BOQrSA' );
define( 'LOGGED_IN_KEY',    '4OwtUPmf>=*z_g6@<UykW?[m1}7}T^]^aOz>2zZLZy<B* =+)SiRiFJ$3nFM`J0$' );
define( 'NONCE_KEY',        '8U!<2XJi#6=7aMJ+MW[Re]H6~@ymyk>qNt=:y-kd.-fk88b$f]h749?gKaopI)1N' );
define( 'AUTH_SALT',        'j^;;J$/gHdK%A]{DZ6%4ld^@v^nk#;O^]zg+U.%n,Cjeft{14O$.2v5.bp53<&S6' );
define( 'SECURE_AUTH_SALT', 'y4=7D4|G`ETx ?|9&HM@[{s9H-89$]@p9_ P]O8BJwy9V(H96f%T9%~tVTMk3/`B' );
define( 'LOGGED_IN_SALT',   '{f@zI)DJu6<NqFinAC+_jY0pfV76,( e7|/MTgBc%UzPsgi9.iX!x^4FO?9c~WpV' );
define( 'NONCE_SALT',       'FnWW Xu?0.:=-xD<*}Ein)s*~Wj/-vyZMIU4MB%Dh9IKTzu?h|rB]-Tau_/RUdc ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
  define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
