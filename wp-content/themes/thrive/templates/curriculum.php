<<<<<<< HEAD
<?php /* Template Name: Curriculum Page */ get_header(); ?>

<section class="curriculum_page">
    <div class="uk-container">
        <h2 class="uk-text-center"><?= get_the_title(); ?></h2>
        
        <div class="search-container">
            <?php get_template_part('searchform'); ?>
        </div>

        <div class="categories">
            <?php
            // Get the current queried object
            $term    = get_queried_object();
            $term_id = ( isset( $term->term_id ) ) ? (int) $term->term_id : 0;

            $categories = get_categories( array(
                'taxonomy'   => 'category', // Taxonomy to retrieve terms for. We want 'category'. Note that this parameter is default to 'category', so you can omit it
                'orderby'    => 'name',
                'parent'     => 0,
                'hide_empty' => 0, // change to 1 to hide categores not having a single post
            ) );
            ?>

            <ul class="nav-switcher" uk-switcher="connect: .my-switcher">
                <?php
                foreach ( $categories as $category ) :
                    $image = get_field('icon', 'category_'. $category->term_id .'');
                    if ( strtolower( $category_name ) != 'uncategorized' ) : ?>
                        <li>
                            <a href="#">
                                <img src="<?= $image['url'] ?>" alt="">
                                <?= $category->name; ?>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div>

        <!-- <div class="filters">
            <div class="uk-flex uk-flex-middle">
                <p>SHOW CLASSES AVAILABLE:</p>
                <div class="input-check">
                    <input type="checkbox" value="online" id="online">
                    <label for="online">Online</label>
                </div>
                <div class="input-check">
                    <input type="checkbox" value="campus" id="campus">
                    <label for="campus">Campus</label>
                </div>
                <div class="input-check">
                    <input type="checkbox" value="summer-school" id="summer-school">
                    <label for="summer-school">Summer School</label>
                </div>
                <div class="input-check">
                    <input type="checkbox" value="part-time" id="part-time">
                    <label for="part-time">Part Time</label>
                </div>
            </div>
        </div> -->

        <div class="content">
            <ul class="uk-switcher my-switcher">
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'career-and-college-readiness',
                            'posts_per_page' => -1,
                            'orderby' => 'name',
                            'order' => 'ASC'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li class="<?= implode(' ', get_field('filter_type')) ?>">
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="credit"><?= get_field('credits'); ?> credits</p>
                                    <p class="meta"><?= implode(', ', get_field('filter_type')) ?></p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'electives',
                            'posts_per_page' => -1,
                            'orderby' => 'name',
                            'order' => 'ASC'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li class="<?= implode(' ', get_field('filter_type')) ?>">
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="credit"><?= get_field('credits'); ?> credits</p>
                                    <p class="meta"><?= implode(', ', get_field('filter_type')) ?></p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'fine-arts',
                            'posts_per_page' => -1,
                            'orderby' => 'name',
                            'order' => 'ASC'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li class="<?= implode(' ', get_field('filter_type')) ?>">
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="credit"><?= get_field('credits'); ?> credits</p>
                                    <p class="meta"><?= implode(', ', get_field('filter_type')) ?></p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'language-arts',
                            'posts_per_page' => -1,
                            'orderby' => 'name',
                            'order' => 'ASC'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li class="<?= implode(' ', get_field('filter_type')) ?>">
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="credit"><?= get_field('credits'); ?> credits</p>
                                    <p class="meta"><?= implode(', ', get_field('filter_type')) ?></p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'math',
                            'posts_per_page' => -1,
                            'orderby' => 'name',
                            'order' => 'ASC'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li class="<?= implode(' ', get_field('filter_type')) ?>">
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="credit"><?= get_field('credits'); ?> credits</p>
                                    <p class="meta"><?= implode(', ', get_field('filter_type')) ?></p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'science',
                            'posts_per_page' => -1,
                            'orderby' => 'name',
                            'order' => 'ASC'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li class="<?= implode(' ', get_field('filter_type')) ?>">
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="credit"><?= get_field('credits'); ?> credits</p>
                                    <p class="meta"><?= implode(', ', get_field('filter_type')) ?></p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'social-studies',
                            'posts_per_page' => -1,
                            'orderby' => 'name',
                            'order' => 'ASC'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li class="<?= implode(' ', get_field('filter_type')) ?>">
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="credit"><?= get_field('credits'); ?> credits</p>
                                    <p class="meta"><?= implode(', ', get_field('filter_type')) ?></p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</section>

<script>
    jQuery(".filters :checkbox").change(function() {
        jQuery(".columned li").hide();
        jQuery(".filters :checkbox:checked").each(function() {
            jQuery(".columned li." + jQuery(this).val()).show();
        });
    });
</script>

=======
<?php /* Template Name: Curriculum Page */ get_header(); ?>

<section class="curriculum_page">
    <div class="uk-container">
        <h2 class="uk-text-center"><?= get_the_title(); ?></h2>

        <div class="categories">
            <?php
            // Get the current queried object
            $term    = get_queried_object();
            $term_id = ( isset( $term->term_id ) ) ? (int) $term->term_id : 0;

            $categories = get_categories( array(
                'taxonomy'   => 'category', // Taxonomy to retrieve terms for. We want 'category'. Note that this parameter is default to 'category', so you can omit it
                'orderby'    => 'name',
                'parent'     => 0,
                'hide_empty' => 0, // change to 1 to hide categores not having a single post
            ) );
            ?>

            <ul class="nav-switcher" uk-switcher="connect: .my-switcher">
                <?php
                foreach ( $categories as $category ) :
                    $image = get_field('icon', 'category_'. $category->term_id .'');
                    if ( strtolower( $category_name ) != 'uncategorized' ) : ?>
                        <li>
                            <a href="#">
                                <img src="<?= $image['url'] ?>" alt="">
                                <?= $category->name; ?>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div>

        <div class="filters">

        </div>

        <div class="content">
            <ul class="uk-switcher my-switcher">
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'career-and-college-readiness',
                            'posts_per_page' => -1,
                            'orderby' => 'name'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li>
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="meta">Filters</p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'electives',
                            'posts_per_page' => -1,
                            'orderby' => 'name'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li>
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="meta">Filters</p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'fine-arts',
                            'posts_per_page' => -1,
                            'orderby' => 'name'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li>
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="meta">Filters</p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'language-arts',
                            'posts_per_page' => -1,
                            'orderby' => 'name'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li>
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="meta">Filters</p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'math',
                            'posts_per_page' => -1,
                            'orderby' => 'name'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li>
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="meta">Filters</p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'science',
                            'posts_per_page' => -1,
                            'orderby' => 'name'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li>
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="meta">Filters</p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
                <li>
                    <?php 
                        $args = array(
                            'post_type' => 'class',
                            'category_name' => 'social-studies',
                            'posts_per_page' => -1,
                            'orderby' => 'name'
                        );

                        $cats = new WP_Query($args);
                    ?>

                    <ul class="columned" uk-accordion>
                        <?php while($cats->have_posts()) : $cats->the_post(); ?>
                            <li>
                                <a class="uk-accordion-title" href="#"><?= get_the_title(); ?></a>
                                <div class="uk-accordion-content">
                                    <p class="meta">Filters</p>
                                    <?= get_field('description'); ?>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</section>

>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
<?php get_footer(); ?>