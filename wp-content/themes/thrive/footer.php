<<<<<<< HEAD
<footer class="footer">
    <div class="uk-container">
        <div class="uk-grid-small" uk-grid>
            <div class="uk-width-1-2@s">
                <a href="<?= bloginfo('url') ?>">
                    <img width="279" src="<?= get_template_directory_uri() ?>/img/logo.svg" alt="ThivePoint Logo">
                </a>
                <div class="col1">
                    <?= get_field('column_1', 'option') ?>
                    <ul class="social">
                        <?php if(get_field('facebook','option')) : ?>
                            <li><a href="<?= get_field('facebook','option') ?>"><img src="<?= get_template_directory_uri() ?>/img/icon-facebook.svg"></a></li>
                        <?php endif; ?>
                        <?php if(get_field('instagram','option')) : ?>
                            <li><a href="<?= get_field('instagram','option') ?>"><img src="<?= get_template_directory_uri() ?>/img/icon-instagram.svg"></a></li>
                        <?php endif; ?>
                        <?php if(get_field('twitter','option')) : ?>
                            <li><a href="<?= get_field('twitter','option') ?>"><img src="<?= get_template_directory_uri() ?>/img/icon-twitter.svg"></a></li>
                        <?php endif; ?>
                        <?php if(get_field('linkedin','option')) : ?>
                            <li><a href="<?= get_field('linkedin','option') ?>"><img src="<?= get_template_directory_uri() ?>/img/icon-linkedin.svg"></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <div class="uk-width-expand@m uk-width-1-2">
                <?= wp_nav_menu(array('menu'=>'Menu 1')) ?>
            </div>
            <div class="uk-width-expand@m uk-width-1-2">
                <?= wp_nav_menu(array('menu'=>'Menu 2')) ?>
            </div>
            <div class="uk-width-expand@m uk-width-1-2">
                <?= wp_nav_menu(array('menu'=>'Menu 3')) ?>
            </div>
        </div>
    </div>
</footer>

<div class="copyright">
    <p><small>&copy; <?= the_date('Y'); ?> <?= bloginfo('title') ?></small></p>
</div>

<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

</body>

=======
<footer class="footer">
    <div class="uk-container">
        <div class="uk-grid-small" uk-grid>
            <div class="uk-width-1-2@m">
                <a href="<?= bloginfo('url') ?>">
                    <img width="279" src="<?= get_template_directory_uri() ?>/img/logo.svg" alt="ThivePoint Logo">
                </a>
                <div class="col1">
                    <?= get_field('column_1', 'option') ?>
                    <ul class="social">
                        <?php if(get_field('facebook','option')) : ?>
                            <li><a href="<?= get_field('facebook','option') ?>"><img src="<?= get_template_directory_uri() ?>/img/icon-facebook.svg"></a></li>
                        <?php endif; ?>
                        <?php if(get_field('instagram','option')) : ?>
                            <li><a href="<?= get_field('instagram','option') ?>"><img src="<?= get_template_directory_uri() ?>/img/icon-instagram.svg"></a></li>
                        <?php endif; ?>
                        <?php if(get_field('twitter','option')) : ?>
                            <li><a href="<?= get_field('twitter','option') ?>"><img src="<?= get_template_directory_uri() ?>/img/icon-twitter.svg"></a></li>
                        <?php endif; ?>
                        <?php if(get_field('linkedin','option')) : ?>
                            <li><a href="<?= get_field('linkedin','option') ?>"><img src="<?= get_template_directory_uri() ?>/img/icon-linkedin.svg"></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <div class="uk-width-expand@m">
                <?= wp_nav_menu(array('menu'=>'Menu 1')) ?>
            </div>
            <div class="uk-width-expand@m">
                <?= wp_nav_menu(array('menu'=>'Menu 2')) ?>
            </div>
            <div class="uk-width-expand@m">
                <?= wp_nav_menu(array('menu'=>'Menu 3')) ?>
            </div>
        </div>
    </div>
</footer>

<div class="copyright">
    <p><small>&copy; <?= the_date('Y'); ?> <?= bloginfo('title') ?></small></p>
</div>

<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

</body>

>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</html>