<?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <div class="event">
        <div class="event-inner">
            <div class="event-banner" style="background: url(<?= get_the_post_thumbnail_url($event) ?>) no-repeat center center / cover; min-height: 260px;">
                <div class="date">
                    <p class="month"><?= get_field('month', $event) ?></p>
                    <p class="day"><?= get_field('day', $event) ?></p>
                </div>
            </div>
            <div class="event-body">
                <h3><?= get_the_title($event) ?></h3>
                <p class="subtitle">
                    <?= get_field('short_description', $event) ?>
                </p>
                <p class="time"><?= get_field('time', $event) ?></p>
                <p class="location"><?= get_field('location', $event) ?></p>
                <div class="uk-child-width-1-2 uk-margin-top" uk-grid>
                    <div>
                        <a href="<?= get_field('register_url', $event) ?>" class="btn orange">Register</a>
                    </div>
                    <div>
                        <a href="<?= get_the_permalink($event) ?>">LEARN MORE <i class="fas fa-angle-right"></i></a>
                    </div>
                </div>
            </div>    
        </div>
    </div>

<?php endwhile; ?>

<?php else: ?>

	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>

<?php endif; ?>
