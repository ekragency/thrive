<<<<<<< HEAD
<?php get_header(); ?>

<main>
	<?php while(have_posts()) : the_post(); ?>
		<div class="single-post">

			<section class="intro">
				<div class="uk-container">
					<div class="uk-child-width-1-2@m uk-flex uk-flex-middle" uk-grid>
						<div>
							<div class="img-container">
								<img src="<?= get_the_post_thumbnail_url(); ?>" alt="">
							</div>
						</div>
						<div>
							<div class="text-container">
								<div class="uk-flex@m uk-flex-middle@m">
									<span>
										<?php 
											$categories = get_the_category();
	 
											if ( ! empty( $categories ) ) {
												echo esc_html( $categories[0]->name );   
											}
										?>
									</span>
									<span><?= get_the_date(); ?></span>
								</div>
								<h2><?= get_the_title(); ?></h2>
								<div class="share">
									<a target="_blank" href="https://twitter.com/intent/tweet?url=<?= get_the_permalink(); ?>"><i class="fab fa-twitter"></i></a>
									<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink(); ?>"><i class="fab fa-facebook"></i></a>
									<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= get_the_permalink(); ?>"><i class="fab fa-linkedin"></i></a>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>

			<section class="main-content">
				<div class="uk-container uk-container-small">
					<?php the_content(); ?>
				</div>
			</section>
		</div>
	<?php endwhile; ?>
</main>

=======
<?php get_header(); ?>

<main>
	<?php while(have_posts()) : the_post(); ?>
		<div class="single-post">
			<section class="featured-image" style="background: url(<?= get_the_post_thumbnail_url(); ?>) no-repeat center center / cover; min-height:384px;"></section>
			<section class="intro">
				<div class="uk-container uk-container-small">
					<p class="uk-flex uk-flex-center">
						<span><?= get_the_date(); ?></span>
						<span>By <?= get_the_author(); ?></span>
					</p>
					<h2><?= get_the_title(); ?></h2>
					<ul>
						<li>Posted in</li>
						<?php wp_list_categories( array(
							'orderby' => 'name',
							'title_li' => ''
						) ); ?> 
					</ul>
				</div>
			</section>
			<?php the_content(); ?>
		</div>
	<?php endwhile; ?>
</main>

>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
<?php get_footer(); ?>