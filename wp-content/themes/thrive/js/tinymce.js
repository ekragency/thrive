<<<<<<< HEAD
(function() {
	tinymce.create('tinymce.plugins.ekrAcfTinyMCE', {
		init: function(editor, url) {
			editor.addButton('ekr_acf_button', {
				text: 'Button',
				icon: 'contrast',
				onclick: function() {
					editor.windowManager.open({
						title: 'Insert Button',
						body: [{
							type: 'textbox',
							name: 'text',
							label: 'Button text'
						},{
							type: 'textbox',
							name: 'link',
							label: 'Button link'
						},{
							type: 'listbox',
							name: 'type',
							label: 'Type',
							values: [
								{ text: 'Gabb Red Button', value: 'default' },
								{ text: 'Gabb White Button', value: 'white' },
								{ text: 'Gabb White Button no border', value: 'white no-border' },
								{ text: 'Underlined No Background', value: 'underline default-link' }
							],
							value: 'default'
						},{
							type: 'listbox',
							name: 'style',
							label: 'Style',
							values: [
								{ text: 'Default', value: '' },
								{ text: 'No Arrow', value: 'no-arrow' },
								{ text: 'Auto Width', value: 'auto-width' },
								{ text: 'Down Arrow', value: 'down' }
							],
							value: 'default'
						}],
						onsubmit: function(e) {
							editor.insertContent(
								'<span><a href="' + e.data.link + '" class="gabb-button ' + e.data.type + ' ' + e.data.style + '">' +
									'<span data-aos="fade">' +
										e.data.text +
									'</span>' +
									'<span class="carret" data-aos="fade-right">.</span>' +
								'</a></span>'
							);
						}
					});
				}
			});
			editor.addButton('ekr_acf_video', {
				text: 'Video',
				icon: 'crop',
				onclick: function() {
					editor.windowManager.open({
						title: 'Insert Video Button',
						body: [{
							type: 'container',
							name: 'container',
							label: '',
							html: '<p style="text-align:center;">This will open a video in a pop up window.</p>'
						},{
							type: 'listbox',
							name: 'type',
							label: 'Type',
							values: [
								{ text: 'Gabb Red Button', value: 'default' },
								{ text: 'Gabb White Button', value: 'white' },
								{ text: 'Gabb White Button no border', value: 'white no-border' },
								{ text: 'Underlined No Background', value: 'underline default-link' }
							],
							value: 'default'
						},{
							type: 'listbox',
							name: 'style',
							label: 'Style',
							values: [
								{ text: 'Default', value: '' },
								{ text: 'No Arrow', value: 'no-arrow' },
								{ text: 'Auto Width', value: 'auto-width' }
							],
							value: 'default'
						},{
							type: 'textbox',
							name: 'buttonText',
							label: 'Button Text'
						},{
							type: 'textbox',
							name: 'link',
							label: 'Youtube ID'
						},{
							type: 'container',
							name: 'orMessage',
							label: '',
							html: '<p style="text-align:center;">OR</p>'
						},{
							type: 'button',
							label: 'Uploaded Video',
							name: 'upload',
							text: 'Upload a Video',
							value: '',
							onclick: function(e) {
								e.preventDefault();
								var self = this;
								self.$el[0].style.overflow = 'hidden';
								var custom_uploader = wp.media.frames.file_frame = wp.media({
									title: 'Choose a Video',
									button: { text: 'Add a Video' },
									multiple: false
								});
								custom_uploader.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									self.state.data.value = attachment.url;
									self.$el[0].innerHTML =
										self.$el[0].innerHTML.replace('Choose a Video', attachment.title);
								});
								custom_uploader.open();
							}
						}],
						onsubmit: function(e) {
							var link = (e.data.link != '') ? 'https://www.youtube-nocookie.com/embed/' + e.data.link : e.data.upload;

							var buttonText = '';
							if(e.data.buttonText == '') {
								buttonText = '&nbsp;';
							} else {
								buttonText = e.data.buttonText;
							}

							var modalType = '';
							if(e.data.link != '') {
								modalType = 'youtube';
							}
							if(e.data.upload) {
								modalType = 'upload';
							}

							var html =
								'<span><a ' +
									'class="gabb-button ' + e.data.type + ' ' + e.data.style + '"' +
									'data-caption="'+modalType+'"' +
									'data-video-url="' + link + '"' +
									'data-add-attr="uk-toggle"' +
									'href="#global-video-modal-'+modalType+'">' +
									'<span data-aos="fade">' +
										buttonText +
									'</span>' +
									'<span class="carret" data-aos="fade-right">.</span>' +
								'</a></span>';

							editor.insertContent(html);
						}
					});
				}
			});
			editor.addButton('ekr_acf_gravity_form', {
				text: 'Form',
				icon: 'orientation',
				onclick: function() {
					editor.windowManager.open({
						title: 'Insert Form',
						body: [{
							type: 'textbox',
							name: 'form_id',
							label: 'Form ID'
						}],
						onsubmit: function(e) {
							editor.insertContent(
								'[gravityform id="' + e.data.form_id + '" title="false" description="false"]'
							);
						}
					});
				}
			});
		}
	});
	tinymce.PluginManager.add('ekr_acf_tinymce', tinymce.plugins.ekrAcfTinyMCE);
})();
=======
(function() {
	tinymce.create('tinymce.plugins.ekrAcfTinyMCE', {
		init: function(editor, url) {
			editor.addButton('ekr_acf_button', {
				text: 'Button',
				icon: 'contrast',
				onclick: function() {
					editor.windowManager.open({
						title: 'Insert Button',
						body: [{
							type: 'textbox',
							name: 'text',
							label: 'Button text'
						},{
							type: 'textbox',
							name: 'link',
							label: 'Button link'
						},{
							type: 'listbox',
							name: 'type',
							label: 'Type',
							values: [
								{ text: 'Gabb Red Button', value: 'default' },
								{ text: 'Gabb White Button', value: 'white' },
								{ text: 'Gabb White Button no border', value: 'white no-border' },
								{ text: 'Underlined No Background', value: 'underline default-link' }
							],
							value: 'default'
						},{
							type: 'listbox',
							name: 'style',
							label: 'Style',
							values: [
								{ text: 'Default', value: '' },
								{ text: 'No Arrow', value: 'no-arrow' },
								{ text: 'Auto Width', value: 'auto-width' },
								{ text: 'Down Arrow', value: 'down' }
							],
							value: 'default'
						}],
						onsubmit: function(e) {
							editor.insertContent(
								'<span><a href="' + e.data.link + '" class="gabb-button ' + e.data.type + ' ' + e.data.style + '">' +
									'<span data-aos="fade">' +
										e.data.text +
									'</span>' +
									'<span class="carret" data-aos="fade-right">.</span>' +
								'</a></span>'
							);
						}
					});
				}
			});
			editor.addButton('ekr_acf_video', {
				text: 'Video',
				icon: 'crop',
				onclick: function() {
					editor.windowManager.open({
						title: 'Insert Video Button',
						body: [{
							type: 'container',
							name: 'container',
							label: '',
							html: '<p style="text-align:center;">This will open a video in a pop up window.</p>'
						},{
							type: 'listbox',
							name: 'type',
							label: 'Type',
							values: [
								{ text: 'Gabb Red Button', value: 'default' },
								{ text: 'Gabb White Button', value: 'white' },
								{ text: 'Gabb White Button no border', value: 'white no-border' },
								{ text: 'Underlined No Background', value: 'underline default-link' }
							],
							value: 'default'
						},{
							type: 'listbox',
							name: 'style',
							label: 'Style',
							values: [
								{ text: 'Default', value: '' },
								{ text: 'No Arrow', value: 'no-arrow' },
								{ text: 'Auto Width', value: 'auto-width' }
							],
							value: 'default'
						},{
							type: 'textbox',
							name: 'buttonText',
							label: 'Button Text'
						},{
							type: 'textbox',
							name: 'link',
							label: 'Youtube ID'
						},{
							type: 'container',
							name: 'orMessage',
							label: '',
							html: '<p style="text-align:center;">OR</p>'
						},{
							type: 'button',
							label: 'Uploaded Video',
							name: 'upload',
							text: 'Upload a Video',
							value: '',
							onclick: function(e) {
								e.preventDefault();
								var self = this;
								self.$el[0].style.overflow = 'hidden';
								var custom_uploader = wp.media.frames.file_frame = wp.media({
									title: 'Choose a Video',
									button: { text: 'Add a Video' },
									multiple: false
								});
								custom_uploader.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									self.state.data.value = attachment.url;
									self.$el[0].innerHTML =
										self.$el[0].innerHTML.replace('Choose a Video', attachment.title);
								});
								custom_uploader.open();
							}
						}],
						onsubmit: function(e) {
							var link = (e.data.link != '') ? 'https://www.youtube-nocookie.com/embed/' + e.data.link : e.data.upload;

							var buttonText = '';
							if(e.data.buttonText == '') {
								buttonText = '&nbsp;';
							} else {
								buttonText = e.data.buttonText;
							}

							var modalType = '';
							if(e.data.link != '') {
								modalType = 'youtube';
							}
							if(e.data.upload) {
								modalType = 'upload';
							}

							var html =
								'<span><a ' +
									'class="gabb-button ' + e.data.type + ' ' + e.data.style + '"' +
									'data-caption="'+modalType+'"' +
									'data-video-url="' + link + '"' +
									'data-add-attr="uk-toggle"' +
									'href="#global-video-modal-'+modalType+'">' +
									'<span data-aos="fade">' +
										buttonText +
									'</span>' +
									'<span class="carret" data-aos="fade-right">.</span>' +
								'</a></span>';

							editor.insertContent(html);
						}
					});
				}
			});
			editor.addButton('ekr_acf_gravity_form', {
				text: 'Form',
				icon: 'orientation',
				onclick: function() {
					editor.windowManager.open({
						title: 'Insert Form',
						body: [{
							type: 'textbox',
							name: 'form_id',
							label: 'Form ID'
						}],
						onsubmit: function(e) {
							editor.insertContent(
								'[gravityform id="' + e.data.form_id + '" title="false" description="false"]'
							);
						}
					});
				}
			});
		}
	});
	tinymce.PluginManager.add('ekr_acf_tinymce', tinymce.plugins.ekrAcfTinyMCE);
})();
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
