<<<<<<< HEAD
jQuery(function ($) {

	$('.slider').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 0,
		speed: 10000,
		arrows: false,
		cssEase: 'linear'
	});

	$('.text-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true
	});

	$('.testimonial-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots: true,
		prevArrow: '<button type="button" class="slick-prev"><span class="arrow-yellow reverse"></span></button>',
		nextArrow: '<button type="button" class="slick-next"><span class="arrow-yellow"></span></button>'
	});

	$('.center').slick({
		centerMode: true,
		centerPadding: '0',
		cssEase: 'linear',
		slidesToShow: 3,
		arrows: false,
		dots: true,
		responsive: [{
			breakpoint: 960,
			settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 1
			}
		}]
	});

	$('.center-small').slick({
		centerMode: true,
		centerPadding: '0',
		slidesToShow: 3,
		arrows: true,
		appendArrows: $('.arrows'),
		prevArrow: '<button type="button" class="slick-prev"><span class="arrow-blue reverse"></span></button>',
		nextArrow: '<button type="button" class="slick-next"><span class="arrow-blue"></span></button>',
		dots: false,
		responsive: [{
			breakpoint: 960,
			settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 1
			}
		}, {
			breakpoint: 600,
			settings: {
				slidesToShow: 1
			}
		}]
	});

	$('.image-slider').slick({
		centerMode: true,
		centerPadding: '0',
		slidesToShow: 5,
		arrows: true,
		appendArrows: $('.arrows'),
		prevArrow: '<button type="button" class="slick-prev"><span class="arrow-blue reverse"></span></button>',
		nextArrow: '<button type="button" class="slick-next"><span class="arrow-blue"></span></button>',
		dots: false,
		responsive: [{
			breakpoint: 960,
			settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '0',
				slidesToShow: 2
			}
		}, {
			breakpoint: 600,
			settings: {
				slidesToShow: 1
			}
		}]
	});

	$(".menu li.menu-item-has-children").append('<i class="fas fa-angle-down"></i>')

=======
jQuery(function ($) {

	$('.slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 0,
		speed: 10000,
		arrows: false,
        cssEase:'linear'
	});

	$('.text-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
		arrows: false,
		dots: true
	});

	$('.testimonial-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
		arrows: true,
		dots: true,
		prevArrow: '<button type="button" class="slick-prev"><span class="arrow-yellow reverse"></span></button>',
		nextArrow: '<button type="button" class="slick-next"><span class="arrow-yellow"></span></button>'
    });

	// Hide Header on on scroll down
	// var didScroll;
	// var lastScrollTop = 0;
	// var delta = 5;
	// var navbarHeight = $('header').outerHeight();

	// $(window).scroll(function (event) {
	// 	didScroll = true;
	// });

	// setInterval(function () {
	// 	if (didScroll) {
	// 		hasScrolled();
	// 		didScroll = false;
	// 	}
	// }, 250);

	// function hasScrolled() {
	// 	var st = $(this).scrollTop();

	// 	// Make sure they scroll more than delta
	// 	if (Math.abs(lastScrollTop - st) <= delta)
	// 		return;

	// 	// If they scrolled down and are past the navbar, add class .nav-up.
	// 	// This is necessary so you never see what is "behind" the navbar.
	// 	if (st > lastScrollTop && st > navbarHeight) {
	// 		// Scroll Down
	// 		$('header').removeClass('nav-down').addClass('nav-up');
	// 	} else {
	// 		// Scroll Up
	// 		if (st + $(window).height() < $(document).height()) {
	// 			$('header').removeClass('nav-up').addClass('nav-down');
	// 		}
	// 	}

	// 	if ($(window).scrollTop() === 0) {
	// 		$('header').removeClass('nav-down');
	// 	}

	// 	lastScrollTop = st;
	// }

>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
});