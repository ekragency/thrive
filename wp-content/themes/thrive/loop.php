<<<<<<< HEAD
<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<?php $id = get_the_id(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="inner" style="background: url(<?= get_the_post_thumbnail_url(); ?>) no-repeat center top / cover;">
			<div class="preview-content">
				<span class="date"><?php the_time('j M Y'); ?></span>
				<h2><a href="<?= get_the_permalink(); ?>"><?= get_the_title(); ?></a></h2>
				
				<?php
					if(get_field('description', $id)) :
						$description = get_field('description', $id);
						$description = limit_string_word_count($description, 35);?>
						<p><?php echo($description . '<br>'); ?></p>
					<?php endif;
				?>
				<a href="<?= get_the_permalink(); ?>">Read more <i class="fas fa-caret-right"></i></a>
			</div>
		</div>

	</article>

<?php endwhile; ?>

<?php else: ?>

	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>

<?php endif; ?>
=======
<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="inner">
			<?php if( get_the_post_thumbnail_url() ) : ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<div class="bg" style="background: url(<?= get_the_post_thumbnail_url(); ?>) no-repeat center top / cover;"></div>
			</a>
			<?php endif; ?>
			<div class="post__excerpt">
				<span class="date"><?php the_time('j M Y'); ?></span>
				<h4>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</h4>
				<p><?php echo wp_trim_words(get_the_excerpt(), 18, ' [...]'); ?></p>
				<a href="<?= get_the_permalink(); ?>">Read More <i class="fas fa-angle-right"></i></a>
			</div>

		</div>

	</article>

<?php endwhile; ?>

<?php else: ?>

	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>

<?php endif; ?>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
