<<<<<<< HEAD
<?php /* Block Name: School Classes */ ?>
<?php //variables
    $title = get_field('title');
    $image = get_field('image');
    $text = get_field('text');
    $plp = get_field('plp_school_links');
    $websites = get_field('college_websites');
    $button_label = get_field('button_label');
    $button_url = get_field('button_url');
?>

<section class="school_classes">
    <div class="uk-container">
        <h2><?= $title ?></h2>
        <div uk-grid>
            <div class="uk-width-2-3@m">
                <div class="bg" style="background: url(<?= $image['url'] ?>) no-repeat center center / cover;">
                    <div class="inner">
                        <?= $text ?>
                    </div>
                </div>
            </div>
            <div class="uk-width-expand@m">
                <div class="text_links">
                    <?= $plp ?>
                </div>
                <div class="text_links">
                    <?= $websites ?>
                </div>
                <div class="wp-block-buttons">
                    <div class="wp-block-button is-style-orange-button">
                        <a href="<?= $button_url ?>" class="wp-block-button__link"><?= $button_label ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
=======
<?php /* Block Name: School Classes */ ?>
<?php //variables
    $title = get_field('title');
    $image = get_field('image');
    $text = get_field('text');
    $plp = get_field('plp_school_links');
    $websites = get_field('college_websites');
    $button_label = get_field('button_label');
    $button_url = get_field('button_url');
?>

<section class="school_classes">
    <div class="uk-container">
        <h2><?= $title ?></h2>
        <div uk-grid>
            <div class="uk-width-2-3@m">
                <div class="bg" style="background: url(<?= $image['url'] ?>) no-repeat center center / cover;">
                    <div class="inner">
                        <?= $text ?>
                    </div>
                </div>
            </div>
            <div class="uk-width-expand@m">
                <div class="text_links">
                    <?= $plp ?>
                </div>
                <div class="text_links">
                    <?= $websites ?>
                </div>
                <div class="wp-block-buttons">
                    <div class="wp-block-button is-style-orange-button">
                        <a href="<?= $button_url ?>" class="wp-block-button__link"><?= $button_label ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</section>