<<<<<<< HEAD
<?php /* Block Name: Locations Block */ ?>
<?php //variables
    $main_image = get_field('main_image');
    $title = get_field('title');
    $text = get_field('text');
    $button = get_field('add_button');
    $button_label = get_field('button_label');
    $button_url = get_field('button_url');
?>

<section class="locations_block" style="background:url(<?= $main_image['url'] ?>) no-repeat center center / cover;">
    <div class="uk-container">
        <div>
            <div class="intro">
                <h2><?= $title ?></h2>
                <?= $text ?>
            </div>
            <div class="places">
                <div class="uk-child-width-1-2@s" uk-grid>
                    <?php while(have_rows('places')) : the_row(); ?>
                        <div>
                            <div class="uk-flex uk-flex-middle">
                                <img src="<?= get_sub_field('icon')['url'] ?>" alt="<?= get_sub_field('icon')['alt'] ?>">
                                <div class="text">
                                    <h4><?= get_sub_field('title') ?></h4>
                                    <p><?= get_sub_field('text') ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <?php if($button) : ?>
                    <div class="wp-block-buttons">
                        <div class="wp-block-button is-style-orange-button">
                            <a href="<?= $button_url ?>" class="wp-block-button__link"><?= $button_label ?></a>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
=======
<?php /* Block Name: Locations Block */ ?>
<?php //variables
    $main_image = get_field('main_image');
    $title = get_field('title');
    $text = get_field('text');
    $button = get_field('add_button');
    $button_label = get_field('button_label');
    $button_url = get_field('button_url');
?>

<section class="locations_block" style="background:url(<?= $main_image['url'] ?>) no-repeat center center / cover;">
    <div class="uk-container">
        <div>
            <h2><?= $title ?></h2>
            <?= $text ?>
            <div class="places">
                <div class="uk-child-width-1-2@s" uk-grid>
                    <?php while(have_rows('places')) : the_row(); ?>
                        <div>
                            <div class="uk-flex uk-flex-middle">
                                <img src="<?= get_sub_field('icon')['url'] ?>" alt="<?= get_sub_field('icon')['alt'] ?>">
                                <div class="text">
                                    <h4><?= get_sub_field('title') ?></h4>
                                    <p><?= get_sub_field('text') ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <?php if($button) : ?>
                    <div class="wp-block-buttons">
                        <div class="wp-block-button is-style-orange-button">
                            <a href="<?= $button_url ?>" class="wp-block-button__link"><?= $button_label ?></a>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</section>