<?php /* Block Name: Image Parallax 2 */ ?>
<?php //variables
    $image_1 = get_field('image_1');
    $image_1_title = get_field('image_1_title');
    $image_1_text = get_field('image_1_text');
    $image_1_url = get_field('image_1_url');
    $image_1_badge = get_field('image_1_badge');
    
    $image_2 = get_field('image_2');
    $image_2_title = get_field('image_2_title');
    $image_2_text = get_field('image_2_text');
    $image_2_url = get_field('image_2_url');
    $image_2_badge = get_field('image_2_badge');
?>

<section class="image_parallax image_parallax_2">
    <div class="uk-container uk-container-small">
        <div class="uk-flex uk-flex-middle">
            <div>
                <div class="img-container" uk-parallax="y: -180"">
                    <div class="shadow"></div>
                    <div class="img" style="background: url(<?= $image_1['url'] ?>) no-repeat center center / cover;"></div>
                    <?php if($image_1_badge) : ?>
                        <img class="badge" src="<?= get_template_directory_uri(); ?>/img/badge.png" alt="Cognia Badge">
                    <?php endif; ?>
                    <div class="text-container">
                        <h3><?= $image_1_title ?></h3>
                        <p><?= $image_1_text ?></p>
                        <a href="<?= $image_1_url ?>">Learn more <i class="fas fa-caret-right"></i></a>
                    </div>
                </div>
            </div>
            <div style="margin-left: auto">
                <div class="img-container last" uk-parallax="y: 180">
                    <div class="shadow"></div>
                    <div class="img" style="background: url(<?= $image_2['url'] ?>) no-repeat center center / cover;"></div>
                    <?php if($image_2_badge) : ?>
                        <img class="badge" src="<?= get_template_directory_uri(); ?>/img/badge.png" alt="Cognia Badge">
                    <?php endif; ?>
                    <div class="text-container">
                        <h3><?= $image_2_title ?></h3>
                        <p><?= $image_2_text ?></p>
                        <a href="<?= $image_2_url ?>">Learn more <i class="fas fa-caret-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="parallax" uk-parallax="y: -80">
        <img class="graphic" src="<?= get_template_directory_uri(); ?>/img/parallax1.svg" alt="Parallax1">
    </div>
</section>