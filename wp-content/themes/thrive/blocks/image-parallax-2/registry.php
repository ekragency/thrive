<?php

acf_register_block(array(
    'name'				=> 'image-parallax-2',
    'title'				=> __('Image Parallax 2'),
    'render_callback'	=> 'my_acf_block_render_callback',
    'category'			=> 'layout',
    'keywords'			=> array( 'parallax' ),
    'mode'              => 'edit',
    'supports'          => array(
        'mode' => false
    )
));

?>