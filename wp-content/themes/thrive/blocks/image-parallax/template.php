<<<<<<< HEAD
<?php /* Block Name: Image Parallax */ ?>
<?php //variables
    $title = get_field('title');
    $text = get_field('text');
    $image_1 = get_field('image_1');
    $image_2 = get_field('image_2');
?>

<section class="image_parallax">
    <div class="uk-container uk-container-small">
        <div class="uk-flex uk-flex-middle">
            <div>
                <img src="<?= $image_1['url'] ?>" alt="<?= $image_1['alt'] ?>" uk-parallax="y: -180">
            </div>
            <div style="margin-left: auto">
                <img src="<?= $image_2['url'] ?>" alt="<?= $image_2['alt'] ?>" uk-parallax="y: 180">
            </div>
        </div>
        <div class="uk-text-center text">
            <h2><?= $title ?></h2>
            <?= $text ?>
        </div>
    </div>
    <div uk-parallax="y: -80">
        <img class="graphic" src="<?= get_template_directory_uri(); ?>/img/parallax1.svg" alt="Parallax1">
    </div>
=======
<?php /* Block Name: Image Parallax */ ?>
<?php //variables
    $title = get_field('title');
    $text = get_field('text');
    $image_1 = get_field('image_1');
    $image_2 = get_field('image_2');
?>

<section class="image_parallax">
    <div class="uk-container uk-container-small">
        <div class="uk-flex uk-flex-middle">
            <div>
                <img src="<?= $image_1['url'] ?>" alt="<?= $image_1['alt'] ?>" uk-parallax="y: -180">
            </div>
            <div style="margin-left: auto">
                <img src="<?= $image_2['url'] ?>" alt="<?= $image_2['alt'] ?>" uk-parallax="y: 180">
            </div>
        </div>
        <div class="uk-text-center text">
            <h2><?= $title ?></h2>
            <?= $text ?>
        </div>
    </div>
    <div uk-parallax="y: -80">
        <img class="graphic" src="<?= get_template_directory_uri(); ?>/img/parallax1.svg" alt="Parallax1">
    </div>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</section>