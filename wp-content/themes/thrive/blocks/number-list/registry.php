<?php

acf_register_block(array(
    'name'				=> 'number-list',
    'title'				=> __('Number List'),
    'render_callback'	=> 'my_acf_block_render_callback',
    'category'			=> 'layout',
    'keywords'			=> array( 'list' ),
    'mode'              => 'edit',
    'supports'          => array(
        'mode' => false
    )
));

?>