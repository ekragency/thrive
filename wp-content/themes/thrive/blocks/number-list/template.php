<?php /* Block Name: Number List */ ?>
<?php //variables
    $title = get_field('title');
    $text = get_field('text');
    $button = get_field('add_button');
    $label = get_field('button_label');
    $url = get_field('button_url');
?>

<section class="number_list">
    <div class="uk-container">
        <div class="uk-text-center intro">
            <h2><?= $title ?></h2>
            <?= $text ?>
        </div>
        <div class="number-list uk-child-width-1-3@s" uk-grid>
            <?php $i = 1; ?>
            <?php while(have_rows('numbers_list')) : the_row(); ?>
                <div>
                    <div class="grid">
                        <p class="number"><?= $i++ ?></p>
                        <div class="text">
                            <h3><?= get_sub_field('title') ?></h3>
                            <h4><?= get_sub_field('subtitle') ?></h4>
                            <?= get_sub_field('text'); ?>
                        </div>
                    </div>
                </div>
            <?php endwhile ?>
        </div>
        <?php if($button) : ?>
            <div class="wp-block-buttons uk-text-center uk-margin-top">
                <div class="wp-block-button is-style-orange-button">
                    <a href="<?= $url ?>" class="wp-block-button__link"><?= $label ?></a>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>