<<<<<<< HEAD
<?php /* Block Name: Column Slider*/ ?>
<?php //variables
    $title = get_field('title');
?>

<section class="column_slider">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-1-3@m">
                <div class="panel">
                    <h2><?= $title ?></h2>
                    <div class="arrows"></div>
                </div>
            </div>
            <div class="uk-width-expand@m">
                <div class="center-small">
                    <?php while(have_rows('slides')) : the_row() ?>
                        <?php //variables
                            $image = get_sub_field('image');
                            $title = get_sub_field('title');
                            $button_label = get_sub_field('button_label');
                            $button_url = get_sub_field('button_url');
                        ?>
                        <div>
                            <div class="tc_banner_block__inner" style="background: url(<?= $image['url'] ?>) no-repeat center center / cover;">
                                <div class="uk-position-bottom-center uk-margin-bottom uk-text-center">
                                    <h2><?= $title ?></h2>
                                    <div class="wp-block-buttons">
                                        <div class="wp-block-button is-style-white-button">
                                            <a href="<?= $button_url ?>" class="wp-block-button__link"><?= $button_label ?> <i class="fas fa-caret-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>
=======
<?php /* Block Name: Column Slider*/ ?>
<?php //variables
    $title = get_field('title');
?>

<section class="column_slider">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-1-3@s">
                <div class="panel">
                    <h2><?= $title ?></h2>
                    <div class="arrows"></div>
                </div>
            </div>
            <div class="uk-width-expand@s">
                <div class="center">
                    <?php while(have_rows('slides')) : the_row() ?>
                        <?php //variables
                            $image = get_sub_field('image');
                            $title = get_sub_field('title');
                            $button_label = get_sub_field('button_label');
                            $button_url = get_sub_field('button_url');
                        ?>
                        <div>
                            <div class="tc_banner_block__inner" style="background: url(<?= $image['url'] ?>) no-repeat center center / cover;">
                                <div class="uk-position-bottom-center uk-margin-bottom uk-text-center">
                                    <h2><?= $title ?></h2>
                                    <div class="wp-block-buttons">
                                        <div class="wp-block-button is-style-white-button">
                                            <a href="<?= $button_url ?>" class="wp-block-button__link"><?= $button_label ?> <i class="fas fa-caret-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    jQuery('.center').slick({
        centerMode: true,
        centerPadding: '0',
        slidesToShow: 3,
        arrows: true,
        appendArrows: jQuery('.arrows'),
        prevArrow: '<button type="button" class="slick-prev"><span class="arrow-blue reverse"></span></button>',
		nextArrow: '<button type="button" class="slick-next"><span class="arrow-blue"></span></button>',
        dots: false,
        responsive: [
            {
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 3
            }
            },
            {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
            }
        ]
        });
</script>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
