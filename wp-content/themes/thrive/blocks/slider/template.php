<<<<<<< HEAD
<?php /* Block Name: Slider Block */ ?>
<?php //variables 
    $title = get_field('title');
?>

<section class="slider_block">
    <div class="uk-container-fluid">
        <div class="uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 100">
            <h2><?= $title ?></h2>
        </div>
        <div class="center" uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 400">
            <?php while(have_rows('slides')) : the_row() ?>
            <?php 
                $background = get_sub_field('background');
                $name = get_sub_field('name');
                $youtube = get_sub_field('youtube');
            ?>
                <div>
                    <div class="slider_block__inner" style="background: url(<?= $background['url'] ?>) no-repeat center center / cover;" uk-lightbox>
                        <a class="uk-button" href="https://www.youtube.com/watch?v=<?= $youtube ?>">
                            <div class="slider_block__name uk-flex uk-flex-middle">
                                <div>
                                    <div class="icon-container">
                                        <i class="fas fa-play"></i>
                                    </div>
                                </div>
                                <div>
                                    <h3><?= $name ?></h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
=======
<?php /* Block Name: Slider Block */ ?>
<?php //variables 
    $title = get_field('title');
?>

<section class="slider_block">
    <div class="uk-container-fluid">
        <div class="uk-text-center">
            <h2><?= $title ?></h2>
        </div>
        <div class="center">
            <?php while(have_rows('slides')) : the_row() ?>
            <?php 
                $background = get_sub_field('background');
                $name = get_sub_field('name');
                $youtube = get_sub_field('youtube');
            ?>
                <div>
                    <div class="slider_block__inner" style="background: url(<?= $background['url'] ?>) no-repeat center center / cover;" uk-lightbox>
                        <a class="uk-button" href="https://www.youtube.com/watch?v=<?= $youtube ?>">
                            <div class="slider_block__name uk-flex uk-flex-middle">
                                <div>
                                    <div class="icon-container">
                                        <i class="fas fa-play"></i>
                                    </div>
                                </div>
                                <div>
                                    <h3><?= $name ?></h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>

<script>
    jQuery('.center').slick({
        centerMode: true,
        centerPadding: '0',
        slidesToShow: 3,
        arrows: false,
        dots: true,
        responsive: [
            {
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 3
            }
            },
            {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
            }
        ]
        });
</script>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
