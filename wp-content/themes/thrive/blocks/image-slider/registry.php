<?php

acf_register_block(array(
    'name'				=> 'image-slider',
    'title'				=> __('Image Slider'),
    'render_callback'	=> 'my_acf_block_render_callback',
    'category'			=> 'layout',
    'keywords'			=> array( 'slider' ),
    'mode'              => 'edit',
    'supports'          => array(
        'mode' => false
    )
));

?>