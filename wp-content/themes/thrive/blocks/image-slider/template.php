<?php /* Block Name: Image Slider */ ?>
<?php //variables
    $title = get_field('title');
    $images = get_field('images');
?>

<section class="image_slider">
    <div class="uk-container">
        <div class="uk-text-center">
            <h2><?= $title ?></h2>
            <div class="arrows"></div>
        </div>
        <div class="image-slider">
            <?php foreach($images as $image) : ?>
                <div>
                    <div class="inner">
                        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                        <figcaption>
                            <?= $image['caption'] ?>
                        </figcaption>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>