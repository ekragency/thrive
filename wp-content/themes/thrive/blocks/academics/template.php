<<<<<<< HEAD
<?php /* Block Name: Academic Icons */ ?>
<?php //variables
    $title = get_field('title');
    $button_label = get_field('button_label');
    $button_url = get_field('button_url');
?>

<section class="academic_icons">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-1-3@s" uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 200">
                <h2><?= $title ?></h2>
                <div class="wp-block-buttons">
                    <div class="wp-block-button is-style-orange-button">
                        <a href="<?= $button_url ?>" class="wp-block-button__link"><?= $button_label ?></a>
                    </div>
                </div>
            </div>
            <div class="uk-width-expand@s">
                <div class="icons uk-child-width-1-3@s uk-child-width-1-2 uk-grid-small" uk-grid uk-scrollspy="target: div; cls:uk-animation-slide-bottom-medium; delay: 150">
                    <?php while(have_rows('icons')) : the_row(); ?>
                        <div>
                            <img src="<?= get_sub_field('icon')['url'] ?>" alt="<?= get_sub_field('icon')['alt'] ?>">
                            <p><?= get_sub_field('text') ?></p>
                        </div>
                    <?php endwhile ?>
                </div>
            </div>
        </div>
    </div>
=======
<?php /* Block Name: Academic Icons */ ?>
<?php //variables
    $title = get_field('title');
    $button_label = get_field('button_label');
    $button_url = get_field('button_url');
?>

<section class="academic_icons">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-1-3@s">
                <h2><?= $title ?></h2>
                <div class="wp-block-buttons">
                    <div class="wp-block-button is-style-orange-button">
                        <a href="<?= $button_url ?>" class="wp-block-button__link"><?= $button_label ?></a>
                    </div>
                </div>
            </div>
            <div class="uk-width-expand@s">
                <div class="icons uk-child-width-1-3@s uk-grid-small" uk-grid>
                    <?php while(have_rows('icons')) : the_row(); ?>
                        <div>
                            <img src="<?= get_sub_field('icon')['url'] ?>" alt="<?= get_sub_field('icon')['alt'] ?>">
                            <p><?= get_sub_field('text') ?></p>
                        </div>
                    <?php endwhile ?>
                </div>
            </div>
        </div>
    </div>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</section>