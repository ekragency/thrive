<<<<<<< HEAD
<?php /* Block Name: Two Column List */ ?>
<?php //variables
    $image = get_field('image');
    $title = get_field('title');
    $text = get_field('text');
    $button = get_field('add_button');
    $label = get_field('button_label');
    $url = get_field('button_url');
?>

<section class="tc_list">
    <div class="uk-container">
        <div class="uk-flex uk-flex-middle">
            <div>
                <img uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 100" src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
            </div>
            <div>
                <div class="text">
                    <h2  uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 200"><?= $title ?></h2>
                    <div  uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 400">
                        <?= $text ?>
                    </div>
                    <?php if($button) : ?>
                        <div class="wp-block-buttons" uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 400">
                            <div class="wp-block-button is-style-orange-button">
                                <a href="<?= $url ?>" class="wp-block-button__link"><?= $label ?></a>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
=======
<?php /* Block Name: Two Column List */ ?>
<?php //variables
    $image = get_field('image');
    $title = get_field('title');
    $text = get_field('text');
    $button = get_field('add_button');
    $label = get_field('button_label');
    $url = get_field('url');
?>

<section class="tc_list">
    <div class="uk-container">
        <div class="uk-flex uk-flex-middle">
            <div>
                <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
            </div>
            <div>
                <div class="text">
                    <h2><?= $title ?></h2>
                    <?= $text ?>
                    <?php if($button) : ?>
                        <div class="wp-block-buttons">
                            <div class="wp-block-button is-style-orange-button">
                                <a href="<?= $url ?>" class="wp-block-button__link"><?= $label ?></a>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</section>