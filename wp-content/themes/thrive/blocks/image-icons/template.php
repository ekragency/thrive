<<<<<<< HEAD
<?php /* Block Name: Image with Icons */ ?>
<?php //variables
    $image = get_field('image');
    $title = get_field('title');
?>

<section class="image_icons">
    <div class="uk-container">
        <div class="img-container">
            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
            <div class="panel">
                <h2><?= $title ?></h2>
                <div class="icons">
                    <div class="uk-child-width-1-4@s uk-child-width-1-2" uk-grid>
                        <?php while(have_rows('icons')) : the_row(); ?>
                            <div>
                                <img src="<?= get_sub_field('icon')['url'] ?>" alt="<?= get_sub_field('icon')['alt'] ?>">
                                <p><?= get_sub_field('description') ?></p>
                            </div>
                        <?php endwhile ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
=======
<?php /* Block Name: Image with Icons */ ?>
<?php //variables
    $image = get_field('image');
    $title = get_field('title');
?>

<section class="image_icons">
    <div class="uk-container">
        <div class="img-container">
            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
            <div class="panel">
                <h2><?= $title ?></h2>
                <div class="icons">
                    <div class="uk-child-width-1-4@s" uk-grid>
                        <?php while(have_rows('icons')) : the_row(); ?>
                            <div>
                                <img src="<?= get_sub_field('icon')['url'] ?>" alt="<?= get_sub_field('icon')['alt'] ?>">
                                <p><?= get_sub_field('description') ?></p>
                            </div>
                        <?php endwhile ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
