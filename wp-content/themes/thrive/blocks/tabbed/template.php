<<<<<<< HEAD
<?php /* Block Name: Tabbed Block */ ?>
<?php //variables
    $title = get_field('title');
    $bg = get_field('background_color');
?>

<section class="tabbed_block" style="background-color: <?= $bg ?>">
    <div class="uk-container">
        <h2 uk-scrollspy="cls: uk-animation-slide-bottom-medium"><?= $title ?></h2>
        <div class="switcher-container">
            <ul class="uk-subnav" uk-switcher="animation: uk-animation-fade" uk-scrollspy="target: li; cls: uk-animation-slide-bottom-medium; delay: 100">
                <?php while(have_rows('tabs')) : the_row() ?>
                    <?php 
                        $label = get_sub_field('label');
                    ?>
                    <li><a href="#"><?= $label ?> <i class="fas fa-caret-down"></i></a></li>
                <?php endwhile; ?>
            </ul>

            <ul class="uk-switcher uk-margin">
                <?php while(have_rows('tabs')) : the_row() ?>
                    <?php  
                        $background = get_sub_field('background');
                        $title = get_sub_field('title');
                        $text = get_sub_field('text');
                        $url = get_sub_field('url');
                        $btnlabel = get_sub_field('btnlabel');
                        $additional = get_sub_field('additional');
                    ?>
                    <li>
                        <div class="uk-switcher__bg" style="background: url(<?= $background['url'] ?>) no-repeat center center / cover;">
                            <div class="uk-switcher__text <?php if($additional) echo 'has-additional' ?>">
                                <h2><?= $title ?></h2>
                                <?= $text ?>
                                <?php if($url || $btnlabel) : ?>
                                <div class="wp-block-buttons">
                                    <div class="wp-block-button is-style-orange-button">
                                        <a href="<?= $url ?>" class="wp-block-button__link"><?= $btnlabel ?></a>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php if($additional) : ?>
                            <div class="people">
                                <?php while(have_rows('people')) : the_row(); ?>
                                    <div>
                                        <img src="<?= get_sub_field('image')['url'] ?>" alt="">
                                        <h4><?= get_sub_field('name') ?></h4>
                                        <p class="subtitle"><?= get_sub_field('position') ?></p>
                                        <a href="mailto:<?= get_sub_field('email') ?>" class="email"><?= get_sub_field('email') ?></a>
                                        <?= get_sub_field('description') ?>
                                    </div>
                                <?php endwhile ?>
                            </div>
                        <?php endif; ?>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
    </div>
=======
<?php /* Block Name: Tabbed Block */ ?>
<?php //variables
    $title = get_field('title');
?>

<section class="tabbed_block">
    <div class="uk-container">
        <h2><?= $title ?></h2>
        <div class="switcher-container">
        <ul class="uk-subnav" uk-switcher="animation: uk-animation-fade">
            <?php while(have_rows('tabs')) : the_row() ?>
                <?php 
                    $label = get_sub_field('label');
                ?>
                <li><a href="#"><?= $label ?> <i class="fas fa-caret-down"></i></a></li>
            <?php endwhile; ?>
        </ul>

        <ul class="uk-switcher uk-margin">
            <?php while(have_rows('tabs')) : the_row() ?>
                <?php  
                    $background = get_sub_field('background');
                    $title = get_sub_field('title');
                    $text = get_sub_field('text');
                    $url = get_sub_field('url');
                    $btnlabel = get_sub_field('btnlabel');
                ?>
                <li>
                    <div class="uk-switcher__bg" style="background: url(<?= $background['url'] ?>) no-repeat center center / cover;"></div>
                    <div class="uk-switcher__text">
                        <h2><?= $title ?></h2>
                        <?= $text ?>
                        <div class="wp-block-buttons">
                            <div class="wp-block-button is-style-orange-button">
                                <a href="<?= $url ?>" class="wp-block-button__link"><?= $btnlabel ?></a>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endwhile; ?>
        </ul>
        </div>
    </div>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</section>