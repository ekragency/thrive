<<<<<<< HEAD
<?php /* Block Name: FAQ Block*/ ?>
<?php //variables
    $image = get_field('image');
    $title = get_field('title');
    $text = get_field('text');
    $button = get_field('add_button');
    $label = get_field('button_label');
    $url = get_field('button_url');
    $contact = get_field('contact');
    $phone = get_field('phone');
    $email = get_field('email');
?>

<?php if(!$image) : ?>
<style>
    .faq_block .faq-questions {
        top: 0;
    }
</style>
<?php endif; ?>

<section class="faq_block">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-1-2@s">
                <div class="text">
                    <h2><?= $title ?></h2>
                    <?= $text ?>
                    <?php if($button) : ?>
                        <div class="wp-block-buttons">
                            <div class="wp-block-button is-style-orange-button">
                                <a href="<?= $url ?>" class="wp-block-button__link"><?= $label ?></a>
                            </div>
                        </div>
                    <?php endif ?>
                    <?php if($contact) : ?>
                        <a href="tel:<?= $phone ?>"><i class="fas fa-phone"></i><?= $phone ?></a>
                        <a href="mailto:<?= $email ?>"><i class="fas fa-envelope"></i><?= $email ?></a>
                    <?php endif ?>
                </div>
            </div>
            <div class="uk-width-expand@s">
                <?php if($image) : ?>
                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                <?php endif; ?>
                <div class="faq-questions">
                    <ul uk-accordion>
                        <?php while(have_rows('questions')) : the_row(); ?>
                            <li>
                                <a class="uk-accordion-title" href="#"><?= get_sub_field('question') ?></a>
                                <div class="uk-accordion-content">
                                    <?= get_sub_field('answer'); ?>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
=======
<?php /* Block Name: FAQ Block*/ ?>
<?php //variables
    $image = get_field('image');
    $title = get_field('title');
    $text = get_field('text');
    $button = get_field('add_button');
    $label = get_field('button_label');
    $url = get_field('button_url');
    $contact = get_field('contact');
    $phone = get_field('phone');
    $email = get_field('email');
?>

<section class="faq_block">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-1-2@s">
                <div class="text">
                    <h2><?= $title ?></h2>
                    <?= $text ?>
                    <?php if($button) : ?>
                        <div class="wp-block-buttons">
                            <div class="wp-block-button is-style-orange-button">
                                <a href="<?= $url ?>" class="wp-block-button__link"><?= $label ?></a>
                            </div>
                        </div>
                    <?php endif ?>
                    <?php if($contact) : ?>
                        <a href="tel:<?= $phone ?>"><i class="fas fa-phone"></i><?= $phone ?></a>
                        <a href="mailto:<?= $email ?>"><i class="fas fa-envelope"></i><?= $email ?></a>
                    <?php endif ?>
                </div>
            </div>
            <div class="uk-width-expand@s">
                <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                <div class="faq-questions">
                    <ul uk-accordion>
                        <?php while(have_rows('questions')) : the_row(); ?>
                            <li>
                                <a class="uk-accordion-title" href="#"><?= get_sub_field('question') ?></a>
                                <div class="uk-accordion-content">
                                    <?= get_sub_field('answer'); ?>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</section>