<?php

acf_register_block(array(
    'name'				=> 'gallery',
    'title'				=> __('Gallery Block'),
    'render_callback'	=> 'my_acf_block_render_callback',
    'category'			=> 'layout',
    'keywords'			=> array( 'gallery' ),
    'mode'              => 'edit',
    'supports'          => array(
        'mode' => false
    )
));

?>