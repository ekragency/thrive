<<<<<<< HEAD
<?php /* Block Name: Gallery Block*/ ?>
<?php //variables
    $image = get_field('image');
    $title = get_field('title');
    $text = get_field('text');
    $gallery = get_field('gallery');
?>

<section class="gallery_block">
    <div class="uk-container">
         <div class="intro uk-grid-large" uk-grid>
             <div class="uk-width-1-3@s">
                 <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
             </div>
             <div class="uk-width-expand@s is-white">
                 <h2><?= $title ?></h2>
                 <?= $text ?>
             </div>
         </div>
         <div class="gallery">
            <?php foreach($gallery as $item) : ?>
                <div>
                    <img src="<?= $item['url'] ?>" alt="<?= $item['alt'] ?>">
                </div>
            <?php endforeach; ?>
         </div>
    </div>
</section>

<script>
    jQuery('.gallery').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: '<button type="button" class="slick-prev"><span class="arrow-blue reverse"></span></button>',
		nextArrow: '<button type="button" class="slick-next"><span class="arrow-blue"></span></button>'
    })
=======
<?php /* Block Name: Gallery Block*/ ?>
<?php //variables
    $image = get_field('image');
    $title = get_field('title');
    $text = get_field('text');
    $gallery = get_field('gallery');
?>

<section class="gallery_block">
    <div class="uk-container">
         <div class="intro uk-grid-large" uk-grid>
             <div class="uk-width-1-3@s">
                 <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
             </div>
             <div class="uk-width-expand@s">
                 <h2><?= $title ?></h2>
                 <?= $text ?>
             </div>
         </div>
         <div class="gallery">
            <?php foreach($gallery as $item) : ?>
                <div>
                    <img src="<?= $item['url'] ?>" alt="<?= $item['alt'] ?>">
                </div>
            <?php endforeach; ?>
         </div>
    </div>
</section>

<script>
    jQuery('.gallery').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: '<button type="button" class="slick-prev"><span class="arrow-blue reverse"></span></button>',
		nextArrow: '<button type="button" class="slick-next"><span class="arrow-blue"></span></button>'
    })
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</script>