<<<<<<< HEAD
<?php /* Block Name: Semester Block */ ?>
<?php //variables
    $title = get_field('title');
    $subtitle = get_field('subtitle');
    $image = get_field('image');
?>

<section class="semester_block">
    <div class="uk-container">
        <div class="uk-child-width-1-2@m" uk-grid>
            <div>
                <div class="inner">
                    <div class="intro">
                        <h2><?= $title ?></h2>
                        <p><?= $subtitle ?></p>
                    </div>
                    <div class="img" style="background: url(<?= $image['url'] ?>) no-repeat center center / cover; height: 350px; width: 100%;"></div>
                </div>
            </div>
            <div>
                <div class="dates">
                    <div class="uk-child-width-1-2@s uk-grid-small" uk-grid>
                        <?php while(have_rows('semester_dates')) : the_row(); ?>
                            <div>
                                <div class="date">
                                    <div class="header">
                                        <p><?= get_sub_field('start_date') ?> - <?= get_sub_field('end_date') ?></p>
                                    </div>
                                    <div class="body uk-text-center">
                                        <p>DOCUMENTS DUE <?= get_sub_field('document_date') ?></p>
                                        <div class="wp-block-buttons">
                                            <div class="wp-block-button is-style-orange-button">
                                                <a href="<?= get_sub_field('apply_url') ?>" class="wp-block-button__link">Appy by <?= get_sub_field('document_date') ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
=======
<?php /* Block Name: Semester Block */ ?>
<?php //variables
    $title = get_field('title');
    $subtitle = get_field('subtitle');
    $image = get_field('image');
?>

<section class="semester_block">
    <div class="uk-container">
        <div class="uk-child-width-1-2@m" uk-grid>
            <div>
                <div class="inner">
                    <div class="intro">
                        <h2><?= $title ?></h2>
                        <p><?= $subtitle ?></p>
                    </div>
                    <div class="img" style="background: url(<?= $image['url'] ?>) no-repeat center center / cover; height: 350px; width: 100%;"></div>
                </div>
            </div>
            <div>
                <div class="dates">
                    <div class="uk-child-width-1-2@s uk-grid-small" uk-grid>
                        <?php while(have_rows('semester_dates')) : the_row(); ?>
                            <div>
                                <div class="date">
                                    <div class="header">
                                        <p><?= get_sub_field('start_date') ?> - <?= get_sub_field('end_date') ?></p>
                                    </div>
                                    <div class="body uk-text-center">
                                        <p>DOCUMENTS DUE <?= get_sub_field('document_date') ?></p>
                                        <div class="wp-block-buttons">
                                            <div class="wp-block-button is-style-orange-button">
                                                <a href="<?= $button_url ?>" class="wp-block-button__link">Appy by <?= get_sub_field('document_date') ?> <i class="fas fa-caret-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</section>