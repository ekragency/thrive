<<<<<<< HEAD
<?php /* Block Name: Banner Block */ ?>
<?php //variables
    $background = get_field('background');
    $title = get_field('title');
    $text = get_field('text');
?>

<section class="banner_block" style="background: url(<?= $background['url'] ?>) no-repeat center center / cover;">
    <div class="uk-container">
        <div class="uk-width-2-3@s">
            <h2 uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 200"><?= $title ?></h2>
            <div uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 400">
                <?= $text ?>
            </div>
        </div>
    </div>
</section>
=======
<?php /* Block Name: Banner Block */ ?>
<?php //variables
    $background = get_field('background');
    $title = get_field('title');
    $text = get_field('text');
?>

<section class="banner_block" style="background: url(<?= $background['url'] ?>) no-repeat center center / cover;">
    <div class="uk-container">
        <div class="uk-width-2-3@s">
            <h2><?= $title ?></h2>
            <?= $text ?>
        </div>
    </div>
</section>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
