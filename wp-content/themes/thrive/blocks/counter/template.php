<<<<<<< HEAD
<?php /* Block Name: Counter Block */ ?>

<section class="counter_block">
    <div class="uk-container">
        <div class="uk-child-width-1-4@m uk-child-width-1-2 uk-text-center" uk-grid>
            <?php while(have_rows('numbers')) : the_row() ?>
                <?php 
                    $number = get_sub_field('number');
                    $label = get_sub_field('label');
                ?>
                <div>
                    <h2 class="counter"><?= $number ?></h2>
                    <p><?= $label ?></p>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/counter/jquery.counterup.min.js"></script>
<script>
    // Number counter
    jQuery('.counter').counterUp({
        time: 2000,
        delay: 10
    });
=======
<?php /* Block Name: Counter Block */ ?>

<section class="counter_block">
    <div class="uk-container">
        <div class="uk-child-width-1-4@m uk-child-width-1-2@s uk-text-center" uk-grid>
            <?php while(have_rows('numbers')) : the_row() ?>
                <?php 
                    $number = get_sub_field('number');
                    $label = get_sub_field('label');
                ?>
                <div>
                    <h2 class="counter"><?= $number ?></h2>
                    <p><?= $label ?></p>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/counter/jquery.counterup.min.js"></script>
<script>
    // Number counter
    jQuery('.counter').counterUp({
        time: 2000,
        delay: 10
    });
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</script>