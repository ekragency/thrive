<<<<<<< HEAD
<?php /* Block Name: Three Column Banner */ ?>
<?php //variables
    $background = get_field('background_image');
?>

<section class="tc_banner_block" style="background: url(<?= $background['url'] ?>) no-repeat center bottom / cover;">
    <div class="uk-container">
        <div class="uk-child-width-1-3@m" uk-scrollspy="cls: uk-animation-fade" uk-grid>

            <?php while(have_rows('columns')) : the_row() ?>
                <?php //variables
                    $image = get_sub_field('image');
                    $title = get_sub_field('title');
                    $button_label = get_sub_field('button_label');
                    $button_url = get_sub_field('button_url');
                ?>
                <div>
                    <div class="tc_banner_block__inner" style="background: url(<?= $image['url'] ?>) no-repeat center center / cover;">
                        <div class="uk-position-bottom-center uk-margin-bottom uk-text-center">
                            <h2><?= $title ?></h2>
                            <div class="wp-block-buttons">
                                <div class="wp-block-button is-style-white-button">
                                    <a href="<?= $button_url ?>" class="wp-block-button__link"><?= $button_label ?> <i class="fas fa-caret-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>

        </div>
    </div>
=======
<?php /* Block Name: Three Column Banner */ ?>
<?php //variables
    $background = get_field('background_image');
?>

<section class="tc_banner_block" style="background: url(<?= $background['url'] ?>) no-repeat center bottom / cover;">
    <div class="uk-container">
        <div class="uk-child-width-1-3@m" uk-grid>

            <?php while(have_rows('columns')) : the_row() ?>
                <?php //variables
                    $image = get_sub_field('image');
                    $title = get_sub_field('title');
                    $button_label = get_sub_field('button_label');
                    $button_url = get_sub_field('button_url');
                ?>
                <div>
                    <div class="tc_banner_block__inner" style="background: url(<?= $image['url'] ?>) no-repeat center center / cover;">
                        <div class="uk-position-bottom-center uk-margin-bottom uk-text-center">
                            <h2><?= $title ?></h2>
                            <div class="wp-block-buttons">
                                <div class="wp-block-button is-style-white-button">
                                    <a href="<?= $button_url ?>" class="wp-block-button__link"><?= $button_label ?> <i class="fas fa-caret-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>

        </div>
    </div>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</section>