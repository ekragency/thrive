<<<<<<< HEAD
<?php /* Block Name: Student Needs */ ?>
<?php //variables
    $image = get_field('image');
    $title = get_field('title');
?>

<section class="student_needs">
    <div class="needs"></div>
    <div class="uk-container uk-text-center">
        <h2 uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 200"><?= $title ?></h2>
        <img uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 400" src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
        <div class="list uk-child-width-1-3@s uk-child-width-1-2" uk-grid uk-scrollspy="cls:uk-animation-slide-bottom-medium; delay: 600">
            <?php while(have_rows('needs_list')) : the_row(); ?>
                <p><?= get_sub_field('text') ?></p>
            <?php endwhile ?>
        </div>
    </div>
=======
<?php /* Block Name: Student Needs */ ?>
<?php //variables
    $image = get_field('image');
    $title = get_field('title');
?>

<section class="student_needs">
    <div class="needs"></div>
    <div class="uk-container uk-text-center">
        <h2><?= $title ?></h2>
        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
        <div class="list uk-child-width-1-3@s" uk-grid>
            <?php while(have_rows('needs_list')) : the_row(); ?>
                <p><?= get_sub_field('text') ?></p>
            <?php endwhile ?>
        </div>
    </div>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</section>