<<<<<<< HEAD
<?php /* Block Name: Two Column Links */ ?>

<section class="tc_links">
    <div class="uk-container uk-container-small">
        <div class="uk-grid-large uk-child-width-1-2@s" uk-grid>
            <?php while(have_rows('links')) : the_row(); ?>
            <div>
                <div class="bg" style="background: url(<?= get_sub_field('background')['url'] ?>) no-repeat center center / cover;">
                    <h2><?= get_sub_field('title') ?></h2>
                    <?= get_sub_field('text') ?>
                    <div class="wp-block-buttons">
                        <div class="wp-block-button is-style-white-button">
                            <a href="<?= get_sub_field('button_url') ?>" class="wp-block-button__link"><?= get_sub_field('button_label') ?> <i class="fas fa-caret-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile ?>
        </div>
    </div>
=======
<?php /* Block Name: Two Column Links */ ?>

<section class="tc_links">
    <div class="uk-container uk-container-small">
        <div class="uk-grid-large uk-child-width-1-2@s" uk-grid>
            <?php while(have_rows('links')) : the_row(); ?>
            <div>
                <div class="bg" style="background: url(<?= get_sub_field('background')['url'] ?>) no-repeat center center / cover;">
                    <h2><?= get_sub_field('title') ?></h2>
                    <?= get_sub_field('text') ?>
                    <div class="wp-block-buttons">
                        <div class="wp-block-button is-style-white-button">
                            <a href="<?= get_sub_field('button_url') ?>" class="wp-block-button__link"><?= get_sub_field('button_label') ?> <i class="fas fa-caret-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile ?>
        </div>
    </div>
>>>>>>> c01029ffff8ac05dab0c2b417ced39d7698cd4ce
</section>