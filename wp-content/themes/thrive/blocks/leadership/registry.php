<?php

acf_register_block(array(
    'name'				=> 'leadership',
    'title'				=> __('Leadership Block'),
    'render_callback'	=> 'my_acf_block_render_callback',
    'category'			=> 'layout',
    'keywords'			=> array( 'leadership' ),
    'mode'              => 'edit',
    'supports'          => array(
        'mode' => false
    )
));

?>