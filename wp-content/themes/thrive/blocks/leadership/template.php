<?php /* Block Name: Leadership Block */ ?>
<?php //variables
    $img1 = get_field('image_1');
    $img2 = get_field('image_2');
    $img3 = get_field('image_3');
    $text = get_field('text');
?>

<section class="leadership_block">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-1-3@s">
                <div class="img1" style="background: url(<?= $img1['url'] ?>) no-repeat center center / cover; min-height: 185px;"></div>
                <div class="img2" style="background: url(<?= $img2['url'] ?>) no-repeat center center / cover; min-height: 185px;"></div>
            </div>
            <div class="uk-width-expand@s">
                <div class="inner">
                    <div class="img3" style="background: url(<?= $img3['url'] ?>) no-repeat center center / cover; min-height: 460px;"></div>
                    <div class="text">
                        <?= $text ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>