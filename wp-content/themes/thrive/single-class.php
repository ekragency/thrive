<?php get_header(); ?>

<main>
	<?php while(have_posts()) : the_post(); ?>
		<div class="single-post class">

			<section class="intro">
				<div class="uk-container">
					<div class="uk-child-width-1-2@m uk-flex uk-flex-middle" uk-grid>
						<div class="text-container">
							<div class="uk-flex@m uk-flex-middle@m">
								<span>
									<?php 
										$categories = get_the_category();
										$categories_array = [];
										foreach($categories as $category) {
											array_push($categories_array, $category->name);
										}
										// sort($categories_array);
										echo(implode($categories_array, ', '));
									?>
								</span>
							</div>
							<h2><?= get_the_title(); ?></h2>
							<?php
								$filters = get_field('filter_type');
								// sort($filters);
								echo('<span>' . strtoupper(implode($filters, ', ')) . '</span>');
							?>
							<hr>
						</div>
					</div>
					<div class="class-description">
						<?= get_field('description'); ?>
					</div>
				</div>
			</section>

			<section class="main-content">
				<div class="uk-container uk-container-small">
					<?php the_content(); ?>
				</div>
			</section>
		</div>
	<?php endwhile; ?>
</main>

<?php get_footer(); ?>