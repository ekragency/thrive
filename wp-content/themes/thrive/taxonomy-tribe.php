<?php get_header(); ?>

	<main role="main">

        <section class="banner-block banner-small">
            <div class="uk-container">
                <div class="banner-small-text">
                    <h1><?php _e( 'Categories for ', 'html5blank' ); single_cat_title(); ?></h1>
                </div>
            </div>
        </section>

        <section class="event-loop">

            <?php get_template_part('loop-event'); ?>

            <?php get_template_part('pagination'); ?>

        </section>

	</main>

<?php get_footer(); ?>
